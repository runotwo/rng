package main

import (
	"context"
	"fmt"
	"go.uber.org/zap"
	"golang.org/x/sync/errgroup"
	"google.golang.org/grpc"
	"net"
	"os"
	"os/signal"
	"rng/rng"
	"rng/rng_service"
	"strconv"
	"syscall"
)

var (
	RngWorkers int64 = 100
	Rng        *rng.Rng
)

var (
	grpcPort   int64 = 9200
	grpcServer *grpc.Server
)

func init() {
	envGrpcPort := os.Getenv("grpc_port")
	if newGrpcPort, err := strconv.ParseInt(envGrpcPort, 10, 64); err == nil {
		grpcPort = newGrpcPort
	}

	envRngWorkers := os.Getenv("rng_workers")
	if newRngWorkers, err := strconv.ParseInt(envRngWorkers, 10, 64); err == nil {
		RngWorkers = newRngWorkers
	}
	Rng = rng.New(RngWorkers)
}

func main() {
	var logger, _ = zap.NewDevelopment()

	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt, syscall.SIGTERM)
	defer signal.Stop(interrupt)

	g, ctx := errgroup.WithContext(ctx)

	g.Go(func() error {
		addr := fmt.Sprintf(":%d", grpcPort)
		ln, err := net.Listen("tcp", addr)
		if err != nil {
			logger.Error("gRPC server: failed to listen", zap.Error(err))
			os.Exit(2)
		}

		var opts []grpc.ServerOption
		grpcServer = grpc.NewServer(opts...)

		rng_service.RegisterRandomNumberGeneratorServer(grpcServer, &RNServer{logger})
		logger.Info("Service started", zap.String("address", addr))
		return grpcServer.Serve(ln)
	})

	select {
	case sig := <-interrupt:
		logger.Info("Exiting program", zap.String("reason", sig.String()))
	case <-ctx.Done():
		break
	}
	logger.Info("received shutdown signal")
	cancel()

	if grpcServer != nil {
		grpcServer.GracefulStop()
	}

	err := g.Wait()
	if err != nil {
		logger.Error("server returning an error", zap.Error(err))
		os.Exit(2)
	}
}
