package main

import (
	"context"
	"fmt"
	"go.uber.org/zap"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"rng/rng_service"
)

type RNServer struct {
	logger *zap.Logger
}

func (s RNServer) GetRandomNumbers(_ context.Context, request *rng_service.RandomNumbersRequest) (resp *rng_service.RandomNumbersResponse, err error) {
	if request.Max < 0 {
		s.logger.Debug("Receive request with negative max", zap.Int32("max", request.Max))
		err = status.Error(codes.InvalidArgument, fmt.Sprintf("max must be positive value, got %d", request.Max))
		return
	}
	if request.Number < 0 {
		s.logger.Debug("Receive request with negative number", zap.Int32("number", request.Number))
		err = status.Error(codes.InvalidArgument, fmt.Sprintf("number must be positive value, got %d", request.Number))
		return
	}

	resp = &rng_service.RandomNumbersResponse{
		Numbers: Rng.GetRandUint32Slice(uint32(request.Number), uint32(request.Max)),
	}
	return
}
