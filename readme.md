Simple grpc service

env vars
```
grpc_port=9200 - Grpc server port
rng_workers=100 - Count of RNG instances in pool
```

Run
```
docker build -t rng .
docker run -p 9200:9200 -d --name rng rng
```
Service available on 0.0.0.0:9200