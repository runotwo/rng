package rng

import (
	"github.com/seehuhn/mt19937"
	"math/rand"
	"time"
)

const (
	MaxRandomNumber = 1<<32 - 1
)

type (
	mtRng struct {
		rng *rand.Rand
	}
	Rng struct {
		c chan *mtRng
	}
)

func newMTRng() *mtRng {
	m := &mtRng{}
	m.rng = rand.New(mt19937.New())
	m.rng.Seed(time.Now().UnixNano())
	return m
}

func (m *mtRng) GetUInt32WithMax(max uint32) uint32 {
	max += 1
	return m.rng.Uint32() / (MaxRandomNumber / max) // for Uniform distribution saving
}

func New(workersCount int64) *Rng {
	ch := make(chan *mtRng, workersCount)
	for i := int64(0); i < workersCount; i++ {
		ch <- newMTRng()
	}
	return &Rng{ch}
}

func (r *Rng) GetRandUint32Slice(count, max uint32) []uint32 {
	res := make([]uint32, count)
	rng := <-r.c
	for i := uint32(0); i < count; i++ {
		res[i] = rng.GetUInt32WithMax(max)
	}
	r.c <- rng
	return res
}
