module rng

go 1.14

require (
	github.com/golang/protobuf v1.4.1
	github.com/seehuhn/mt19937 v0.0.0-20191220121156-d07252b9f9df
	go.uber.org/zap v1.15.0
	golang.org/x/sync v0.0.0-20190423024810-112230192c58
	google.golang.org/grpc v1.30.0
	google.golang.org/protobuf v1.25.0
)
